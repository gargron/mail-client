package com.zeonfederated.mailclient.fetchers;
import com.zeonfederated.mailclient.Message;
import com.zeonfederated.mailclient.MessageList;


public interface Fetcher {
	public boolean connect(FetcherTransport transport);
	public boolean authenticate(String user, String pass);
	public MessageList all();
	public Message find(int id);
	public void close();
}
