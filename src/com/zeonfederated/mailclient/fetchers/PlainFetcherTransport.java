package com.zeonfederated.mailclient.fetchers;

import java.net.Socket;

public class PlainFetcherTransport implements FetcherTransport {

	@Override
	public Socket getSocket(String host, int port) throws Exception {
		return new Socket(host, port);
	}

}
