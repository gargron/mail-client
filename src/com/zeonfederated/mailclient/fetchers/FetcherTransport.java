package com.zeonfederated.mailclient.fetchers;

import java.net.Socket;

public interface FetcherTransport {
	public Socket getSocket(String host, int port) throws Exception;
}
