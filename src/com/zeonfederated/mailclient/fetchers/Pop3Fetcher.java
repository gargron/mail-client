package com.zeonfederated.mailclient.fetchers;
import java.net.*;
import java.util.ArrayList;
import java.io.*;

import com.zeonfederated.mailclient.Message;
import com.zeonfederated.mailclient.MessageList;

public class Pop3Fetcher implements Fetcher {

	private String host;
	private int port;
	
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	
	public Pop3Fetcher(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	/**
	 * Versuch, beim Server einzuloggen
	 */
	
	@Override
	public boolean authenticate(String user, String pass) {
		out.println("USER " + user);

		if(isOK()) {
			out.println("PASS " + pass);

			if(isOK()) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public MessageList all() {
		MessageList messages = new MessageList();
		
		try {
			out.println("LIST");
			
			if(isOK()) {
				String line = in.readLine();
				ArrayList<Integer> ids = new ArrayList<Integer>();
				Message curMsg;
				
				while(! line.equals(".")) {
					ids.add(Integer.parseInt(line.split(" ")[0]));
					line = in.readLine();
				}
				
				for(Integer id : ids) {
					curMsg = find(id);
					messages.add(curMsg);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return messages;
	}

	/**
	 * Einzelne Nachricht laden
	 */
	
	@Override
	public Message find(int id) {
		String multiline = "";
		
		out.println("RETR " + id);
		
		try {
			if(isOK()) {
				String line = in.readLine();
				
				while(! line.equals(".")) {
					multiline += line + "\n";
					line       = in.readLine();
				}
				
				return Message.parse(multiline);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private boolean isOK() {
		try {
			String res = in.readLine();
			System.out.println(res);
			return res.startsWith("+OK");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public void close() {
		try {
			// out.println("RSET");
			out.println("QUIT");
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean connect(FetcherTransport transport) {
		try {
			this.socket = transport.getSocket(host, port);
			this.in     = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			this.out    = new PrintWriter(this.socket.getOutputStream(), true);
			
			if (isOK()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

}
