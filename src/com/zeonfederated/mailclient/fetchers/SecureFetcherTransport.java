package com.zeonfederated.mailclient.fetchers;

import java.net.Socket;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SecureFetcherTransport implements FetcherTransport {

	@Override
	public Socket getSocket(String host, int port) throws Exception {
		SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
		SSLSocket sslSocket      = (SSLSocket) factory.createSocket(host, port);
		sslSocket.startHandshake();
		
		return (Socket) sslSocket;
	}

}
