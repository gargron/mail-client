package com.zeonfederated.mailclient.fetchers;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;

import com.zeonfederated.mailclient.Message;
import com.zeonfederated.mailclient.MessageList;

public class JavamailFetcher implements Fetcher {
	
	private String host;
	private int port;
	private String user;
	private String password;
	private Store store;
	private Folder inbox;
	
	public JavamailFetcher(String host, int port, String user, String password) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;
	}

	@Override
	public boolean connect(FetcherTransport transport) {
		Properties props = new Properties();
		Session session  = Session.getDefaultInstance(props);
		
		try {
			if(transport instanceof SecureFetcherTransport) {
				store = session.getStore("pop3s");
			} else if(transport instanceof PlainFetcherTransport) {
				store = session.getStore("pop3");
			} else {
				return false;
			}
		} catch (NoSuchProviderException e) {
			return false;
		}
		
		try {
			store.connect(host, port, user, password);
			inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_WRITE);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	@Override
	public boolean authenticate(String user, String pass) {
		// Wird in der Implementation nicht benutzt
		return true;
	}

	@Override
	public MessageList all() {
		MessageList list = new MessageList();
		int count;
		
		try {
			count = inbox.getMessageCount();
		} catch (MessagingException e) {
			e.printStackTrace();
			count = 0;
		}
		
		if(count == 0) {
			return list;
		}
		
		for(int i = 0; i < count; i++) {
			list.add(find(i + 1));
		}
		
		return list;
	}

	@Override
	public Message find(int id) {
		MimeMessage parsed;
		
		try {
			parsed = (MimeMessage) inbox.getMessage(id);
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
		
		return new Message(parsed);
	}

	@Override
	public void close() {
		try {
			inbox.close(true);
			store.close();
		} catch (MessagingException e) {}
	}

}
