package com.zeonfederated.mailclient;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

public class Message {
	private MimeMessage parsed;
	private String source;
	private boolean opened;
	
	/**
	 * Initializiert die Instanz mit leeren Eigenschaften. Die Unterlage
	 * für die Klasse bietet MimeMessage von Javamail
	 */
	public Message() {
		this.source = null;
		this.parsed = new MimeMessage(Session.getDefaultInstance(new Properties()));
	}
	
	/**
	 * Initializiert die Instanz aufgrund eines schon existierenden MimeMessage
	 */
	public Message(MimeMessage parsed) {
		ByteArrayOutputStream sourceBytes = new ByteArrayOutputStream();
		
		this.parsed = parsed;
		
		try {
			parsed.writeTo(sourceBytes);
		} catch (MessagingException| IOException e) {
			e.printStackTrace();
		}
		
		this.source = sourceBytes.toString();
	}
	
	/**
	 * Erstellt eine Instanz ausgehend von einem String (dem Quelltext der Nachricht)
	 */
	public static Message parse(String str) {
		Message message = new Message();
		
		if(str == null) {
			return null;
		}

		try {
			message.source = str;
			message.parsed = new MimeMessage(Session.getDefaultInstance(new Properties()), new ByteArrayInputStream(str.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return message;
	}
	
	public String getSource() {
		if(source == null) {
			ByteArrayOutputStream sourceBytes = new ByteArrayOutputStream();
			
			try {
				parsed.writeTo(sourceBytes);
			} catch (MessagingException| IOException e) {
				e.printStackTrace();
			}
			
			source = sourceBytes.toString();
		}
		
		return source;
	}
	
	public String getFormattedDate() {
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		Date date = getDate();
		
		if(date == null) {
			return null;
		}
		
		return df.format(date);
	}
	
	public String getShortFrom() {
		String from = getFullFrom();
		
		if(from == null) {
			return null;
		}
		
		String[] fromParts = from.split("<");
		
		if(fromParts[0].length() > 0) {
			return fromParts[0];
		} else {
			return fromParts[1].substring(0, -2);
		}
	}
	
	public String getSubject() {
		try {
			return parsed.getSubject();
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Date getDate() {
		try {
			return parsed.getSentDate();
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getId() {
		try {
			return parsed.getMessageID();
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getBody() {
		try {
			Object multipart = parsed.getContent();
			
			if(multipart instanceof String) {
				return multipart + "";
			}
			
			return  ((MimeMultipart) multipart).getBodyPart(0).getContent() + "";
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getQuotedBody()
	{
		String body    = getBody();
		String[] lines = body.split("\n");
		String quoted  = "";
		
		for(String line : lines) {
			quoted += "> " + line + "\n";
		}
		
		return quoted;
	}
	
	public String getFullFrom() {
		try {
			String from = parsed.getFrom()[0] + "";
			
			try {
				return MimeUtility.decodeText(from);
			} catch (UnsupportedEncodingException e) {
				return from;
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getEmailFrom() {
		String from = getFullFrom();
		
		if(from == null) {
			return null;
		}
		
		String[] fromParts = from.split("<");
		
		if(fromParts.length == 2) {
			return "<" + fromParts[1];
		} else {
			return from;
		}
	}
	
	public Address[] getRecipients(boolean all) {
		try {
			if(all) {
				return parsed.getAllRecipients();
			}
			
			return parsed.getRecipients(javax.mail.Message.RecipientType.TO);
		} catch (MessagingException e) {
			// For some reason, this chokes on the @uni-jena.de@minet addresses
			// So we will ignore this error
			// e.printStackTrace();
			return null;
		}
	}
	
	public String getTo() {
		String to = "";
		Address[] rcpts = getRecipients(false);
		
		if(rcpts == null) {
			return null;
		}
		
		for(int i = 0; i < rcpts.length; i++) {
			to += rcpts[i] + ((i + 1) < rcpts.length ? ", " : "");
		}
		
		return to;
	}
	
	public String getSentDate(Locale locale) {
		DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", locale);
		Date date = getDate();
		
		if(date == null) {
			return null;
		}
		
		return df.format(date);
	}
	
	public boolean getOpened() {
		return opened;
	}
	
	public void setFrom(String from) {
		try {
			parsed.setFrom(from);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void setTo(String to) {
		try {
			parsed.setRecipients(javax.mail.Message.RecipientType.TO, to);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void setBcc(String bcc) {
		try {
			parsed.setRecipients(javax.mail.Message.RecipientType.BCC, bcc);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void setSubject(String subject) {
		try {
			parsed.setSubject(subject);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void setBody(String body) {
		try {
			parsed.setText(body);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void setSentDate(Date date) {
		try {
			parsed.setSentDate(date);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}
}
