package com.zeonfederated.mailclient.frames;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.zeonfederated.mailclient.*;
import com.zeonfederated.mailclient.fetchers.*;
import com.zeonfederated.mailclient.layout.SpringUtilities;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements MouseListener {
	
	/**
	 * Inhalt der E-mails Tabelle
	 */
	private DefaultTableModel tableModel;
	
	/**
	 * Die Tabelle selbst
	 */
	private JTable table;
	
	/**
	 * Text der Statusleiste
	 */
	private JLabel statusTxt;
	
	/**
	 * Anzeigepanel für Nachrichten
	 */
	private JPanel viewPane;
	
	/**
	 * Hier sind die ganzen Nachrichtenobjekte
	 */
	private MessageList list;
	
	/**
	 * Datenbank der E-mails
	 */
	private MessageProvider provider;
	
	/**
	 * Konfiguration
	 */
	private Config config;
	
	/**
	 * Anzahl ungelesener Nachrichten
	 */
	private int unopenedCount;

	/**
	 * Im Konstruktor wird die ganze GUI aufgestellt, die
	 * ActionListeners für die GUI-Elemente deklariert, und
	 * am Ende werden die E-mails gleich gecheckt.
	 */
	public MainFrame() {
		setTitle("E-mails");
		setSize(600, 300);
		setLocationRelativeTo(null);
		
		addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				provider.disconnect();
				System.exit(0);
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		config   = new Config();
		provider = new MessageProvider();
		provider.connect();
		
		table = new JTable() {
			
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				
				// Fette Schrift wenn ungelesen
				if(! list.get(row).getOpened()) {
					c.setFont(c.getFont().deriveFont(Font.BOLD));
				}
				
				return c;
			}
			
		};
		
		JPanel mainPane         = new JPanel(new GridLayout(1, 2));
		JScrollPane tablePane   = new JScrollPane(table);
		JPanel statusPane       = new JPanel();
		viewPane                = new JPanel(new BorderLayout());
		JPanel headersPane      = new JPanel(new SpringLayout());
		JTextArea messageTxt    = new JTextArea(10, 70);
		JScrollPane messagePane = new JScrollPane(messageTxt);
		JToolBar toolbar        = new JToolBar();
		
		JLabel fromLabel    = new JLabel("Von:");
		JLabel fromTxt      = new JLabel();
		JLabel toLabel      = new JLabel("An:");
		JLabel toTxt        = new JLabel();
		JLabel subjectLabel = new JLabel("Betreff:");
		JLabel subjectTxt   = new JLabel();
		
		headersPane.add(fromLabel);
		headersPane.add(fromTxt);
		headersPane.add(toLabel);
		headersPane.add(toTxt);
		headersPane.add(subjectLabel);
		headersPane.add(subjectTxt);
		
		SpringUtilities.makeCompactGrid(headersPane, 3, 2, 5, 5, 5, 2);
		
		JButton refreshBtn = new JButton("Empfangen");
		JButton composeBtn = new JButton("Verfassen");
		JButton replyBtn   = new JButton("Antworten");
		JButton confBtn    = new JButton("Einstellungen");
		JButton quitBtn    = new JButton("Schließen");
		statusTxt          = new JLabel("Bereit");
		
		statusPane.add(statusTxt);
		toolbar.add(refreshBtn);
		toolbar.add(composeBtn);
		toolbar.add(replyBtn);
		toolbar.add(confBtn);
		toolbar.add(quitBtn);
		
		messageTxt.setLineWrap(true);
		messageTxt.setWrapStyleWord(true);
		messageTxt.setTabSize(2);
		messageTxt.setFont(new Font("DialogInput", Font.PLAIN, 12));
		messageTxt.setEditable(false);
		
		viewPane.add(headersPane, BorderLayout.NORTH);
		viewPane.add(messagePane, BorderLayout.CENTER);
		
		replyBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				final int index = table.getSelectedRow();
				
				if(index == -1) {
					return;
				}
				
				final Message selected = list.get(index);
				
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						JFrame composeFrame = new ComposeFrame(config, selected);
						composeFrame.setVisible(true);
					}
					
				});
			}
			
		});
		
		refreshBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				statusTxt.setText("Laden...");
				
				Thread fetchThread = new Thread() {

					@Override
					public void run() {
						fetchMail();
					}
					
				};
		
				fetchThread.start();
			}
			
		});
		
		composeBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						JFrame composeFrame = new ComposeFrame(config);
						composeFrame.setVisible(true);
					}
					
				});
			}
			
		});
		
		confBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFrame confFrame = new SettingsFrame(config);
				confFrame.setVisible(true);
			}
			
		});
		
		quitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				provider.disconnect();
				System.exit(0);
			}
			
		});
		
		tableModel = new DefaultTableModel() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
				return false;
			}
			
		};
		
		tableModel.addColumn("ID");
		tableModel.addColumn("Date sent");
		tableModel.addColumn("From");
		tableModel.addColumn("Subject");
		
		table.setModel(tableModel);
		table.removeColumn(table.getColumn("ID"));
		table.setCellSelectionEnabled(false);
		table.setColumnSelectionAllowed(false);
		table.setRowSelectionAllowed(true);
		table.setShowGrid(false);
		table.setShowHorizontalLines(true);
		table.addMouseListener(this);
		
		mainPane.add(tablePane);
		mainPane.add(viewPane);
		
		add(toolbar, BorderLayout.NORTH);
		add(mainPane, BorderLayout.CENTER);
		add(statusPane, BorderLayout.SOUTH);
		
		pack();
		
		if(! config.has("fetch.user")) {
			// Wenn Empfangskonfiguration fehlt, öffne Einstellungen
			statusTxt.setText("Empfangen ist noch nicht konfiguriert");
			
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					JFrame confFrame = new SettingsFrame(config);
					confFrame.setVisible(true);
				}
				
			});
		} else {
			// Sonst, lade existierende/neue E-mails
			statusTxt.setText("Laden...");
			
			Thread fetchThread = new Thread() {
				
				@Override
				public void run() {
					loadMail();
					fetchMail();
				}
				
			};
			
			fetchThread.start();
		}
	}
	
	/**
	 * Hier startet der Client. Das Hauptfenster wird "später"
	 * erstellt und gezeigt, damit nichts hängt.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				MainFrame main = new MainFrame();
				main.setVisible(true);
			}
			
		});
	}
	
	/**
	 * Versucht, die E-mails zu laden und in die Datenbank zu speichern
	 */
	private void fetchMail() {
		// Hier mit Javamail:
		// Fetcher fetcher   = new JavamailFetcher(config.get("fetch.host", "localhost"), Integer.parseInt(config.get("fetch.port", "995")), config.get("fetch.user", ""), config.getEncrypted("fetch.pass"));
		
		// Oder hier mit der eigenen Implementation:
		Fetcher fetcher   = new Pop3Fetcher(config.get("fetch.host", "localhost"), Integer.parseInt(config.get("fetch.port", "995")));
		
		boolean hadErrors = false;
		FetcherTransport transport;
		
		if(config.get("fetch.ssl", "").equals("yes")) {
			transport = new SecureFetcherTransport();
		} else {
			transport = new PlainFetcherTransport();
		}

		if(! fetcher.connect(transport)) {
			statusTxt.setText("Konnte mit dem Mailhost nicht verbinden");
			hadErrors = true;
		} else {
			if(! fetcher.authenticate(config.get("fetch.user", ""), config.getEncrypted("fetch.pass"))) {
				statusTxt.setText("Login beim Mailhost fehlgeschlagen");
				fetcher.close();
				hadErrors = true;
			} else {
				provider.fetchAndSave(fetcher);
				fetcher.close();
			}
		}
		
		loadMail();
		
		if(! hadErrors) {
			statusTxt.setText("Bereit");
		}
	}
	
	/**
	 * Lädt E-mails aus der Datenbank und füllt die Tabelle
	 */
	private void loadMail() {
		list          = provider.all();
		unopenedCount = 0;
		tableModel.setRowCount(0);

		for(int i = 0, len = list.size(); i < len; i++) {
			Message msg = list.get(i);
			
			if(!msg.getOpened()) {
				unopenedCount++;
			}
			
			tableModel.addRow(new Object[] { msg.getId(), msg.getFormattedDate(), msg.getShortFrom(), msg.getSubject() });
		}
		
		if(unopenedCount > 0) {
			setTitle("(" + unopenedCount + ") E-mails");
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		final int index = table.getSelectedRow();
		
		if(index == -1) {
			return;
		}
		
		final Message selected = list.get(index);
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JLabel fromTxt    = (JLabel) ((JPanel) viewPane.getComponent(0)).getComponent(1);
				JLabel toTxt      = (JLabel) ((JPanel) viewPane.getComponent(0)).getComponent(3);
				JLabel subjectTxt = (JLabel) ((JPanel) viewPane.getComponent(0)).getComponent(5);
				JTextArea body    = (JTextArea) ((JScrollPane) viewPane.getComponent(1)).getViewport().getView();
				
				fromTxt.setText(selected.getFullFrom());
				toTxt.setText(selected.getTo());
				subjectTxt.setText(selected.getSubject());
				body.setText(selected.getBody());
				
				if(! selected.getOpened()) {
					// Nur wenn die Nachricht zuvor nicht geöffnet wurde,
					// markiere sie als geöffnet und speichere das in der
					// Datenbank
					selected.setOpened(true);
					table.updateUI();
					provider.markOpened(selected);
					unopenedCount--;
					setTitle("(" + unopenedCount + ") E-mails");
				}
			}
			
		});
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
