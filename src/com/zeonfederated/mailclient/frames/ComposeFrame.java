package com.zeonfederated.mailclient.frames;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;
import javax.swing.border.EtchedBorder;

import com.zeonfederated.mailclient.Config;
import com.zeonfederated.mailclient.Message;
import com.zeonfederated.mailclient.layout.SpringUtilities;
import com.zeonfederated.mailclient.senders.JavamailSender;
import com.zeonfederated.mailclient.senders.Sender;
import com.zeonfederated.mailclient.senders.SmtpSender;

@SuppressWarnings("serial")
public class ComposeFrame extends JFrame {

	private Config config;
	private JLabel statusTxt;
	private JTextField to;
	private JTextField cc;
	private JTextField bcc;
	private JTextField subject;
	private JTextArea body;

	public ComposeFrame(Config config) {
		this.config = config;
		setupUI();
	}
	
	public ComposeFrame(Config config, Message replyToMessage) {
		this(config);
		setTitle("E-mail Antwort verfassen");
		to.setText(replyToMessage.getFullFrom());
		subject.setText("Re: " + replyToMessage.getSubject());
		body.setText("\n\n" + replyToMessage.getQuotedBody());
		body.setCaretPosition(0);
		body.requestFocusInWindow();
	}
	
	private void setupUI() {
		setTitle("E-mail verfassen");
		setLocationRelativeTo(null);
		
		JPanel form      = new JPanel(new SpringLayout());
		JPanel container = new JPanel(new BorderLayout());
		JToolBar actions = new JToolBar();
		JPanel status    = new JPanel();
		
		JLabel toLabel      = new JLabel("An:");
		JLabel ccLabel      = new JLabel("CC:");
		JLabel bccLabel     = new JLabel("BCC:");
		JLabel subjectLabel = new JLabel("Betreff:");
		
		to      = new JTextField(20);
		cc      = new JTextField(20);
		bcc     = new JTextField(20);
		subject = new JTextField(20);
		
		toLabel.setLabelFor(to);
		ccLabel.setLabelFor(cc);
		bccLabel.setLabelFor(bcc);
		subjectLabel.setLabelFor(subject);

		form.add(toLabel);
		form.add(to);
		form.add(ccLabel);
		form.add(cc);
		form.add(bccLabel);
		form.add(bcc);
		form.add(subjectLabel);
		form.add(subject);
		form.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		
		SpringUtilities.makeCompactGrid(form, 4, 2, 5, 5, 5, 2);
		
		body = new JTextArea(10, 70);
		JScrollPane bodyPane = new JScrollPane(body);
		
		bodyPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		body.setLineWrap(true);
		body.setWrapStyleWord(true);
		body.setTabSize(2);
		body.setFont(new Font("DialogInput", Font.PLAIN, 12));
		
		JButton sendBtn   = new JButton("Senden");
		JButton cancelBtn = new JButton("Abbrechen");
		
		actions.add(sendBtn);
		actions.add(cancelBtn);
		
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});
		
		sendBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				statusTxt.setText("Senden...");
				
				Thread sendThread = new Thread() {
					
					@Override
					public void run() {
						sendMail();
					}
					
				};
				
				sendThread.start();
			}
			
		});
		
		statusTxt = new JLabel("Bereit");
		status.add(statusTxt);
		
		container.add(form, BorderLayout.NORTH);
		container.add(bodyPane);
		
		add(actions, BorderLayout.NORTH);
		add(container);
		add(status, BorderLayout.SOUTH);
		pack();
	}
	
	private void sendMail() {
		// Eigene Implementation:
		Sender sender = new SmtpSender(config.get("send.host", "localhost"), Integer.parseInt(config.get("send.port", "25")));
		
		// Javamail:
		// Sender sender = new JavamailSender(config.get("send.host", "localhost"), Integer.parseInt(config.get("send.port", "25")));
		Message msg = new Message();
		
		msg.setFrom(config.get("my.name") + " <" + config.get("my.email") + ">");
		msg.setTo(to.getText() + ", " + cc.getText());
		msg.setBcc(bcc.getText());
		msg.setSubject(subject.getText());
		msg.setBody(body.getText());
		msg.setSentDate(new Date());
		
		if(! sender.connect()) {
			statusTxt.setText("Konnte mit dem Mailhost nicht verbinden");
			return;
		}
		
		if(sender.send(msg)) {
			sender.close();
		} else {
			statusTxt.setText("Konnte die Nachricht nicht absenden");
			return;
		}

		statusTxt.setText("OK");
		setVisible(false);
	}
}
