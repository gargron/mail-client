package com.zeonfederated.mailclient.frames;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;

import com.zeonfederated.mailclient.Config;
import com.zeonfederated.mailclient.layout.SpringUtilities;

@SuppressWarnings("serial")
public class SettingsFrame extends JFrame {

	public SettingsFrame(final Config config) {
		setTitle("Einstellungen");
		setLocationRelativeTo(null);
		
		JPanel form        = new JPanel();
		JPanel formGeneral = new JPanel(new SpringLayout());
		JPanel formFetch   = new JPanel(new SpringLayout());
		JPanel formSend    = new JPanel(new SpringLayout());
		
		form.setLayout(new BoxLayout(form, BoxLayout.Y_AXIS));
		formGeneral.setBorder(BorderFactory.createTitledBorder("Allgemein"));
		formFetch.setBorder(BorderFactory.createTitledBorder("Empfang"));
		formSend.setBorder(BorderFactory.createTitledBorder("Senden"));
		
		JLabel nameL     = new JLabel("Name:");
		JLabel emailL    = new JLabel("E-mail:");
		
		JLabel fetchHostL   = new JLabel("Host:");
		JLabel fetchPortL   = new JLabel("Port:");
		JLabel fetchUserL   = new JLabel("Nutzer:");
		JLabel fetchPassL   = new JLabel("Kennwort:");
		JLabel fetchUseSslL = new JLabel("Sicherheit:");
		
		JLabel sendHostL = new JLabel("Host:");
		JLabel sendPortL = new JLabel("Port:");
		
		final JTextField name  = new JTextField(config.get("my.name"), 20);
		final JTextField email = new JTextField(config.get("my.email"), 20);
		
		final JTextField fetchHost  = new JTextField(config.get("fetch.host"), 20);
		final JTextField fetchPort  = new JTextField(config.get("fetch.port"), 20);
		final JTextField fetchUser  = new JTextField(config.get("fetch.user"), 20);
		final JTextField fetchPass  = new JPasswordField(config.getEncrypted("fetch.pass"), 20);
		final JCheckBox fetchUseSsl = new JCheckBox("SSL nutzen", config.get("fetch.ssl", "").equals("yes"));
		
		final JTextField sendHost = new JTextField(config.get("send.host"), 20);
		final JTextField sendPort = new JTextField(config.get("send.port"), 20);
		
		nameL.setLabelFor(name);
		emailL.setLabelFor(email);
		
		fetchHostL.setLabelFor(fetchHost);
		fetchPortL.setLabelFor(fetchPort);
		fetchUserL.setLabelFor(fetchUser);
		fetchPassL.setLabelFor(fetchPass);
		fetchUseSslL.setLabelFor(fetchUseSsl);
		
		sendHostL.setLabelFor(sendHost);
		sendPortL.setLabelFor(sendPort);
		
		formGeneral.add(nameL);
		formGeneral.add(name);
		formGeneral.add(emailL);
		formGeneral.add(email);
		
		formFetch.add(fetchHostL);
		formFetch.add(fetchHost);
		formFetch.add(fetchPortL);
		formFetch.add(fetchPort);
		formFetch.add(fetchUserL);
		formFetch.add(fetchUser);
		formFetch.add(fetchPassL);
		formFetch.add(fetchPass);
		formFetch.add(fetchUseSslL);
		formFetch.add(fetchUseSsl);
		
		formSend.add(sendHostL);
		formSend.add(sendHost);
		formSend.add(sendPortL);
		formSend.add(sendPort);
		
		SpringUtilities.makeCompactGrid(formGeneral, 2, 2, 5, 5, 5, 2);
		SpringUtilities.makeCompactGrid(formFetch, 5, 2, 5, 5, 5, 2);
		SpringUtilities.makeCompactGrid(formSend, 2, 2, 5, 5, 5, 2);
		
		JToolBar toolbar  = new JToolBar();
		JButton saveBtn   = new JButton("Speichern");
		JButton cancelBtn = new JButton("Abbrechen");
		
		saveBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				config.put("my.name", name.getText());
				config.put("my.email", email.getText());
				
				config.put("fetch.host", fetchHost.getText());
				config.put("fetch.port", fetchPort.getText());
				config.put("fetch.user", fetchUser.getText());
				config.putEncrypted("fetch.pass", fetchPass.getText());
				config.put("fetch.ssl", fetchUseSsl.getSelectedObjects() != null ? "yes" : "no");
				
				config.put("send.host", sendHost.getText());
				config.put("send.port", sendPort.getText());
		
				config.save();
				setVisible(false);
			}
			
		});
		
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
			
		});
		
		toolbar.add(saveBtn);
		toolbar.add(cancelBtn);
		form.add(formGeneral);
		form.add(formFetch);
		form.add(formSend);
		
		add(toolbar, BorderLayout.NORTH);
		add(form);
		pack();
	}
}
