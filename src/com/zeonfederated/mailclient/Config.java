package com.zeonfederated.mailclient;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class Config {
	private Properties properties;
	
	public Config() {
		properties = new Properties();
		load();
	}
	
	private void load() {
		FileInputStream in;
		
		try {
			in = new FileInputStream("config");
			properties.load(in);
		} catch (IOException e) {
			// Whatever
		}
	}
	
	public void save() {
		FileOutputStream out;
		
		try {
			out = new FileOutputStream("config");
			properties.store(out, "---No Comment---");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void put(String key, String value) {
		properties.setProperty(key, value);
	}
	
	/**
	 * Diese Methode speichert einen Wert genau wie einfaches "put",
	 * aber davor wird der Wert durch AES verschlüsselt. Dabei dient
	 * der lokale Hostname als Schlüssel
	 */
	public void putEncrypted(String key, String value) {
		SecretKeySpec keySpec = getKey();
		Cipher cipher;
		byte[] encrypted;
		
		try {
			cipher = Cipher.getInstance("AES");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
			return;
		}
		
		try {
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, cipher.getParameters());
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			return;
		}
		
		try {
			encrypted = cipher.doFinal(value.getBytes());
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
			return;
		}
		
		put(key, bytesToHexString(encrypted));
	}
	
	private String bytesToHexString(byte[] bytes) {
		return DatatypeConverter.printHexBinary(bytes);
	}
	
	private byte[] hexStringToBytes(String string) throws IllegalArgumentException {
		if(string == null) {
			throw new IllegalArgumentException();
		}
		
		return DatatypeConverter.parseHexBinary(string);
	}
	
	public boolean has(String key) {
		String val = get(key);
		return (val != null) && !val.equals("");
	}
	
	public String get(String key) {
		return properties.getProperty(key);
	}
	
	public String getEncrypted(String key) {
		SecretKeySpec keySpec = getKey();
		Cipher cipher;
		byte[] decrypted;
		
		try {
			cipher = Cipher.getInstance("AES");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			decrypted = cipher.doFinal(hexStringToBytes(get(key)));
		} catch (IllegalBlockSizeException | BadPaddingException | IllegalArgumentException e) {
			// Das mag nicht verschlüsselt gewesen sein, geben wir es also einfach aus
			return get(key);
		}
		
		return new String(decrypted);
	}
	
	public String get(String key, String defaultValue) {
		String val = properties.getProperty(key);
		
		if(val == null || val.equals("")) {
			return defaultValue;
		}
		
		return val;
	}
	
	private SecretKeySpec getKey() {
		String keyOrigin;
		byte[] keyHash;
		MessageDigest sha;
		
		try {
			sha = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
		try {
			// Hash: Fully qualified host name
			keyOrigin = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			// Fallback: class name
			keyOrigin = this.getClass().getName();
		}
		
		keyHash = sha.digest(keyOrigin.getBytes());
		keyHash = Arrays.copyOf(keyHash, 16);
		SecretKeySpec key = new SecretKeySpec(keyHash, "AES");
		return key;
	}
}
