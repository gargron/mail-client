package com.zeonfederated.mailclient.senders;

import java.io.ByteArrayInputStream;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import com.zeonfederated.mailclient.Message;

public class JavamailSender implements Sender {
	
	private Session session;
	private String host;
	private int port;
	
	public JavamailSender(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Override
	public boolean connect() {
		// Wird mit Javamail nicht zum eigentlichen "verbinden" genutzt
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		session = Session.getDefaultInstance(props);
		return true;
	}

	@Override
	public boolean authenticate(String user, String pass) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean send(Message message) {
		MimeMessage transportMsg;
		
		try {
			// Leider kann man die Session eines MimeMessage nicht später in der Laufzeit
			// ändern, deswegen müssen wir ein neues vom alten erstellen
			 transportMsg = new MimeMessage(session, new ByteArrayInputStream(message.getSource().getBytes()));
			 Transport.send(transportMsg);
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	@Override
	public void close() {
		// Wird hier gar nicht gebraucht
	}

}
