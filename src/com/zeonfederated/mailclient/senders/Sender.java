package com.zeonfederated.mailclient.senders;
import com.zeonfederated.mailclient.Message;


public interface Sender {
	public boolean connect();
	public boolean authenticate(String user, String pass);
	public boolean send(Message message);
	public void close();
}
