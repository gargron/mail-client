package com.zeonfederated.mailclient.senders;
import java.net.*;
import java.util.Locale;
import java.io.*;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;

import com.zeonfederated.mailclient.Message;

public class SmtpSender implements Sender {

	private String host;
	private int port;
	
	protected Socket socket;
	protected BufferedReader in;
	protected PrintWriter out;
	
	public SmtpSender(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public boolean connect() {
		try {
			this.socket = new Socket(host, port);
			this.in     = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			this.out    = new PrintWriter(this.socket.getOutputStream(), true);
			
			if(!isReturnCode(220)) {
				close();
				return false;
			}
			
			out.println("HELO " + InetAddress.getLocalHost().getHostAddress());
			
			if(!isReturnCode(250)) {
				close();
				return false;
			}
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}
	
	private boolean isReturnCode(int code) {
		try {
			String res = in.readLine();
			System.out.println(res);
			return res.startsWith("" + code);
		} catch (IOException e) {
			return false;
		}
	}
	
	@Override
	public boolean authenticate(String user, String pass) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean send(Message message) {
		out.println("MAIL FROM: " + message.getEmailFrom());
		
		if(!isReturnCode(250)) {
			return false;
		}
		
		for(Address rcpt : message.getRecipients(true)) {
			out.println("RCPT TO: <" + rcpt + ">");
			
			if(!isReturnCode(250)) {
				return false;
			}
		}
		
		out.println("DATA");
		
		if(!isReturnCode(354)) {
			return false;
		}
		
		try {
			MimeUtility.encode(socket.getOutputStream(), "7bit");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		out.println("From: " + message.getFullFrom());
		out.println("To: " + message.getTo());
		out.println("Date: " + message.getSentDate(Locale.ENGLISH));
		out.println("Subject: " + message.getSubject());
		out.println("User-Agent: Super Mail Client One Million");
		out.println("MIME-Version: 1.0");
		out.println("Content-Type: text/plain; charset=utf-8");
		out.println("Content-Transfer-Encoding: 7bit");
		out.println(message.getBody());
		out.println(".");
		
		if(!isReturnCode(250)) {
			return false;
		}
		
		return true;
	}
	
	public void close() {
		try {
			out.println("QUIT");
			socket.close();
		} catch (IOException e) {}
	}

}
