package com.zeonfederated.mailclient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.zeonfederated.mailclient.fetchers.Fetcher;

public class MessageProvider {

	private static final String CSDriver   = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String dbURLCS    = "jdbc:derby:mail";
	private static final int schemaVersion = 7;
	
	private Connection connection;
	
	public boolean connect() {
		try {
			Class.forName(CSDriver).newInstance();
			connection = DriverManager.getConnection(dbURLCS + ";create=true");
			connection.setAutoCommit(false);
			checkSchemaVersion(schemaVersion);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void disconnect() {
		try {
			DriverManager.getConnection(dbURLCS + ";shutdown=true");
		} catch (SQLException sqle) {
			if (!"08006".equals(sqle.getSQLState())) {
				sqle.printStackTrace();
			}
		}
	}
	
	/**
	 * Lädt E-mails vom Fetcher und versucht sie in der Datenbank
	 * zu speichern. Als primary key dient die UID der Nachricht. Wenn
	 * eine Nachricht schon gespeichert ist, wird sie übersprungen.
	 * 
	 * Mit POP3 gibt es keinen Weg, "nur neue" Nachrichten zu laden, ohne
	 * dass man dafür immer alte Nachrichten vom Server löscht. Und um die UIDs
	 * zu vergleichen, braucht man sowieso immer RETR für jede Nachricht,
	 * effizienter geht es wohl nicht.
	 */
	public void fetchAndSave(Fetcher fetcher) {
		if(connection == null) {
			return;
		}
		
		try {
			MessageList newMessages = fetcher.all();
			PreparedStatement ps    = connection.prepareStatement("INSERT INTO messages VALUES (?, ?, ?, ?, ?, ?)");
			
			for(Message msg : newMessages) {
				try {
					ps.setString(1, msg.getId());
					ps.setString(2, msg.getEmailFrom());
					ps.setString(3, msg.getTo());
					ps.setInt(4, 0);
					ps.setInt(5, (int) (msg.getDate().getTime() / 1000));
					ps.setString(6, msg.getSource());
					ps.executeUpdate();
				} catch (SQLException msgE) {
					// Schon in Datenbank, weiter
				} catch (NullPointerException nE) {
					// Fehler beim Parsen. Wahrscheinlich schlecht geformter Quelltext,
					// besser weitermachen als abbrechen
				}
			}
			
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public MessageList all() {
		MessageList messages = new MessageList();
		
		if(connection == null) {
			return messages;
		}
		
		try {
			ResultSet stored = connection.createStatement().executeQuery("SELECT source, opened FROM messages ORDER BY date DESC");
			Message curr;
			
			while(stored.next()) {
				curr = Message.parse(stored.getString(1));
				
				if(curr == null) {
					// Fehler beim Parsen, das ist nicht gut
					continue;
				}
				
				curr.setOpened(stored.getBoolean(2));
				messages.add(curr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return messages;
	}
	
	public void markOpened(Message message) {
		if(connection == null) {
			return;
		}
		
		try {
			PreparedStatement ps = connection.prepareStatement("UPDATE messages SET opened = true WHERE id = ?");
			ps.setString(1, message.getId());
			ps.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Überprüft ob die Datenbankschema auf dem letzten Stand ist, sonst
	 * erstellt die Tabellen neu. Nützlich bei ständiger Entwicklung.
	 */
	private void checkSchemaVersion(int version) {
		try {
			Statement s = connection.createStatement();
			
			try {
				s.executeQuery("SELECT * FROM dbschema");
			} catch (SQLException e) {
				// dbschema does not exist
				s.execute("CREATE TABLE dbschema (version INT)");
				createTables();
			}
			
			ResultSet schema      = s.executeQuery("SELECT version FROM dbschema");
			PreparedStatement psU = connection.prepareStatement("UPDATE dbschema SET version = ?");
			PreparedStatement psI = connection.prepareStatement("INSERT INTO dbschema values (?)");
			
			if(! schema.next()) {
				psI.setInt(1, version);
				psI.executeUpdate();
				connection.commit();
				return;
			} else {
				int oldVersion = schema.getInt(1);
				
				if(version == oldVersion) {
					connection.commit();
					return;
				}
			}
			
			try {
				s.execute("DROP TABLE messages");
			} catch (SQLException e) {
				// if messages did not exist, we don't even care
			}
			
			createTables();
			psU.setInt(1, version);
			psU.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private boolean createTables() {
		try {
			Statement s = connection.createStatement();
			s.execute("CREATE TABLE messages(id VARCHAR(100) PRIMARY KEY, from_email VARCHAR(100), to_email VARCHAR(100), opened BOOLEAN, date INT, source CLOB)");
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
