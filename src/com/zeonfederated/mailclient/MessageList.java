package com.zeonfederated.mailclient;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class MessageList extends ArrayList<Message> {

	public int countUnopened() {
		int count = 0;
		
		for(Message msg : this) {
			if(!msg.getOpened()) {
				count++;
			}
		}
		
		return count;
	}
}
